Annotation Store Module:
------------------------

Annotation Store Module is mainly focused on storing the video
annotation data [Open Video Annotation Library] and mirador
data locally in drupal. This modules creates the annotation
data as an entity in drupal in the name of "annotation_store".

The entities which are created is listed at "/annotation_store/list".

Annotation list links provided at '/admin/content'.

This information includes Annotation text, type, URI, user, created
and changed time stamp.

NOTE: To work around open video annotation in drupal 8 using OVA
module. Refer the module at https://www.drupal.org/project/ova.

Future Enhancements:
--------------------

1. To work with update and delete annotation for mirador and open
video annotation.
